/// @description 

// Every step (pre)
clamp(ply.sinceLastJump++, 0, 9001);

/// COLLISION & GRAVITY ///
if (!place_meeting(x, y+1, o_collidable_static)) {
  y += 1;
}

/// CHECK FOR INPUT ///
if (keyboard_check_pressed(ply.key.up)) {
  // ** JUMP **
  if ( (ply.sinceLastJump > ply.jumpStepLimit) ) {
    y -= ply.spd.v + 2;
  }
  // reset jump counter
  ply.sinceLastJump = 0;
}
if (keyboard_check(ply.key.left)) {
  // <- LEFT <-    
  x -= ply.spd.h;
}
if (keyboard_check(ply.key.down)) {
  // __ CROUCH __
  y += ply.spd.v;
}
if (keyboard_check(ply.key.right)) {
  // -> RIGHT ->
  x += ply.spd.h;
}
if (keyboard_check(ply.key.atk0)) {
  // Attack: Basic X
  ply.jump(x, y);
}
if (keyboard_check_pressed(ply.key.atk1)) {
  // Attack: Slice \\
  ply.attack(1);
  //TODO
}
if (keyboard_check(ply.key.atk2)) {
  // Attack: Sweep ((
  ply.attack(2);
}

// Every step (post)
