{
  "function": 0,
  "channels": [
    {"colour":4282401023,"visible":true,"points":[
        {"x":0.0,"y":0.0,},
        {"x":0.07392996,"y":-0.3737739,},
        {"x":0.118677042,"y":-0.5205361,},
        {"x":0.23929961,"y":-0.5771812,},
        {"x":0.340466917,"y":-0.32388562,},
        {"x":0.517509758,"y":-0.0228728689,},
        {"x":0.71206224,"y":-0.005368932,},
        {"x":1.0,"y":0.0,},
      ],"controlPoints":[
        {"x":0.42,"y":0.0,},
        {"x":0.58,"y":1.0,},
      ],"resourceVersion":"1.0","name":"curve1","tags":[],"resourceType":"GMAnimCurveChannel",},
    {"colour":4294934783,"visible":true,"points":[
        {"x":0.0,"y":0.0,},
        {"x":0.07392996,"y":-0.187919468,},
        {"x":0.118677042,"y":-0.328859061,},
        {"x":0.23929961,"y":-0.3557047,},
        {"x":0.340466917,"y":-0.154362425,},
        {"x":0.517509758,"y":0.22147648,},
        {"x":0.71206224,"y":0.2147651,},
        {"x":1.0,"y":0.2147651,},
      ],"controlPoints":[
        {"x":0.42,"y":0.0,},
        {"x":0.58,"y":1.0,},
      ],"resourceVersion":"1.0","name":"curve2","tags":[],"resourceType":"GMAnimCurveChannel",},
  ],
  "parent": {
    "name": "Animation Curves",
    "path": "folders/Animation Curves.yy",
  },
  "resourceVersion": "1.1",
  "name": "ac_test",
  "tags": [],
  "resourceType": "GMAnimCurve",
}