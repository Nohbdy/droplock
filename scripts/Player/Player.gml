// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Player(_name) : Entity() constructor {
  name = _name;
  input_frozen = false;
  jumpStepLimit = 20;
  sinceLastJump = 0;
  instanceLayer = "i_collidableScenery";
  spd =
  {
    h : 1,
    v : 1
  }
  jump = function(cx, cy) {
    if (sinceLastJump > jumpStepLimit) {
      layer_sequence_create(instanceLayer, cx+8, cy+8, sq_atk0);
      sinceLastJump = 0;
    }
  }
  attack = function(attack_type) {
    switch(attack_type) {
      case 0:
        break;
      case 1:
        jumpStepLimit = (jumpStepLimit == 20 ? jumpStepLimit = 1 : jumpStepLimit = 20);
        break;
      case 2:
        break;
    }
  }
  // Keys (as in keyboard)
  key = {
    up    : ord("W"),
    left  : ord("A"),
    down  : ord("S"),
    right : ord("D"),
    atk0  : ord("K"),
    atk1  : ord("L"),
    atk2  : ord("M"),
    
  }
}