// Game class
// Handles game-level system events
function Game() constructor {
  // Debug Message
  dbgmsg = function(_msg) {
    if (mode.debug) {
      show_debug_message(_msg);
    }
  }
  save = function() {
    dbgmsg("SAVING");
    //TODO
    // Get filename from autoGen or player
    
    // Save
    
    // Set "lastSave"
  }
  load = function() {
    dbgmsg("LOADING");
    //TODO
    // Get filename from lastSave or player
    
    // Load
  }
  // Player (struct)
  player = {
    // List of players
    list : ds_list_create(),
    // # of players
    count : 0,
    // Player Join
    join : function(_player) {
      var ID = count + 1;
      dbgmsg("PLAYER_" + string(ID) + "_JOIN");
      ds_list_add(list, _player);
      count++;
    },
    // Player Drop
    drop : function(_player) {
      dbgmsg("PLAYER_" + string(ID) + "_DROP");
      var playerToDrop = ds_list_find_index(list, _player);
      ds_list_delete(list, playerToDrop);
      count--;
    },
    // Get player from list
    get : function(_playerName) {
      dbgmsg("PLAYER_" + string(ID) + "_DROP");
      return list[| _player];
    }
  }
  // Modes
  mode = {
    // Debug mode (used to switch messages off/on)
    debug : true,
  }
}